package INF102.lab3.numberGuesser;

public class MyGeniusGuesser implements IGuesser {

    @Override
    public int findNumber(RandomNumber number) { // O(log n)

        int lowerbound = number.getLowerbound(); // O(1)
        int upperbound = number.getUpperbound(); // O(1)

        while (lowerbound <= upperbound) { // O(log n)
            int guess = lowerbound + (upperbound - lowerbound) / 2; // O(1)
            int result = number.guess(guess); // O(1)

            if (result == 0) { // O(1)
                return guess; // O(1)
            } else if (result == 1) { // O(1)
                upperbound = guess - 1; // O(1)
            } else if (result == -1) { // O(1)
                lowerbound = guess + 1; // O(1)
            }
        }
        return -1; // O(1)
    }

}
