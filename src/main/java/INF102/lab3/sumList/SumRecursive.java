package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum { // O(n)

    @Override
    public long sum(List<Long> list) {
        if (list.isEmpty())
            return 0;
        else {
            long last = list.get(list.size() - 1);
            list.remove(list.size() - 1);
            return last + sum(list);
        }
    }
}
