package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) { // O(log n)
        if (numbers == null || numbers.isEmpty()) { // O(1)
            throw new IllegalArgumentException("Empty list"); // O(1)
        }
        return findPeak(numbers, 0, numbers.size() - 1); // O(log n)
    }

    private int findPeak(List<Integer> numbers, int left, int right) { // O(log n)
        if (left == right) { // O(1)
            return numbers.get(left); // O(1)
        }

        int mid = left + (right - left) / 2; // O(1)

        if (numbers.get(mid) > numbers.get(mid + 1)) { // O(1)
            return findPeak(numbers, left, mid); // O(log n)
        } else { // O(1)
            return findPeak(numbers, mid + 1, right); // O(log n)
        }
    }

}
